import React, { Component } from "react";
import "./App.css";
import Planetlist from "./Views/Planetlist";

class App extends Component {
  componentDidMount() {}
  render() {
    return (
      <div className="App">
        <Planetlist />
      </div>
    );
  }
}

export default App;
