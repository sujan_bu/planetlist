/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Component } from "react";
import "bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import "react-table-6/react-table.css";
import "../Css/main.css";
import axios from "axios";
import "@fortawesome/fontawesome-free/css/all.min.css";
import "bootstrap-css-only/css/bootstrap.min.css";
import "mdbreact/dist/css/mdb.css";

class PlanetList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      planet_data: [],
      searchvalue: "",
    };
    this.fetch_data = this.fetch_data.bind(this);
    this.changeHandler = this.changeHandler.bind(this);
    this.handleSearch = this.handleSearch.bind(this);
  }

  componentDidMount() {
    this.fetch_data();
  }

  fetch_data(state) {
    axios({
      url: "https://swapi-gql-adform.herokuapp.com",
      method: "post",
      data: {
        query:
          "{allPlanets {planets{name}, planets{diameter}, planets{population}, planets{gravity}, planets{climates}}}",
      },
    }).then((result) => {
      let planetResult = result.data;
      this.setState({
        planet_data: planetResult["data"]["allPlanets"]["planets"],
      });
      // console.log(this.state.planet_data);
    });
  }

  changeHandler = (e) => {
    e.preventDefault();
    this.setState({
      searchvalue: e.target.value,
    });
  };

  handleSearch = (e) => {
    e.preventDefault();
    let planetSearch = this.state.planet_data;
    let search = this.state.searchvalue.trim().toLowerCase();

    if (search.length > 0) {
      planetSearch = planetSearch.filter(function (planet) {
        return planet.name.toLowerCase().match(search);
      });
      this.setState({
        planet_data: planetSearch,
      });
    }
  };

  render() {
    return (
      <div id="planetPage" className="container-fluid m-0 p-0">
        <div className="mb-5">
          <nav className="navbar navbar-light bg-dark">
            <a href="" className="navbar-brand text-white text-uppercase">
              Planet List
            </a>
            <form onSubmit={this.handleSearch} className="form-inline">
              <input
                id="search"
                ref="search"
                value={this.state.searchvalue}
                onChange={this.changeHandler}
                className="form-control mr-2"
                type="search"
                placeholder="Search"
                aria-label="Search"
              />
              <button
                onSubmit={this.handleSearch}
                className="btn btn-outline-success"
                type="submit"
              >
                Search
              </button>
            </form>
          </nav>
        </div>
        <div className="container">
          <div className="row">
            {this.state.planet_data.map((planet, index) => {
              return (
                <div
                  key={index}
                  className="col-xs-12 col-sm-12 col-md-6 col-lg-4 col-xl-4 mb-4"
                >
                  <div className="card">
                    <div className="view rounded view-cascade gradient-card-header blue-gradient text-center py-3">
                      <h2 className="card-header-title mb-3 text-white font-weigh-bold text-uppercase namePlan">
                        {planet.name}
                      </h2>
                    </div>

                    <div className="card-body card-body-cascade text-center">
                      <div id="list-container d-flex align-items-center">
                        <div className="list-item-container mb-1">
                          <p className="list-title text-uppercase m-0 p-0">
                            population
                          </p>
                          <div className="value">{planet.population}</div>
                        </div>
                        <div className="list-item-container mb-1">
                          <p className="list-title text-uppercase m-0 p-0">
                            Diameter
                          </p>
                          <div className="value">{planet.diameter}</div>
                        </div>
                        <div className="list-item-container mb-1">
                          <p className="list-title text-uppercase m-0 p-0">
                            Gravity
                          </p>
                          <div className="value">{planet.gravity}</div>
                        </div>
                        <div className="list-item-container mb-1">
                          <p className="list-title text-uppercase m-0 p-0">
                            Climate
                          </p>
                          <div className="value">{planet.climate}</div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              );
            })}
          </div>
        </div>
      </div>
    );
  }
}

export default PlanetList;
