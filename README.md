Clone the project in your local directory using git clone with https.
In order to run react projects, you must have node installed in you system.
Once after you clone the repository, go to the folder "planetlist" in the terminal and enter the command "npm start".
The server will start up in the local host on port 3000.
You must be able to see the list of planets fetched from the GraphQl.
The list of all planets is shown in the form of cards.
Each planets with its corresponding details are displayed as a seperate card item.
